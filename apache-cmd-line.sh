#! /bin/sh

#======================#
#	  Help         #
#======================#
USAGE="Usage: `basename $0` (create|delete|enable|disable) vhostname [port] [path/to/project]";

show_help() {
    echo 'Available actions:
    create - creates a new project. Includes folder structure and database (optional).
    delete - delete all project files and drops database (optional). Asks for confirmation.
    enable - enables selected virtual host.
    disable - disables selected virtual host
    help - shows this message.';    
}

#======================#
#     RESULT STYLE     #
#======================#
YELLOW=$(tput setf 6)
RED=$(tput setf 4)
GREEN=$(tput setf 2)
RESET=$(tput sgr0)
TOEND=$(tput hpa $(tput cols))$(tput cub 6)

#======================#
#	SETTINGS       #
#======================#
port=80;
make_index=0;
projects="/var/www";
hosts="/etc/apache2/sites-available";
hosts_file="/etc/hosts";
enabled_hosts='/etc/apache2/sites-enabled';
user=$(whoami);

#======================#
#	FUNCTIONS      #
#======================#

echo_warning(){
  echo -n "${YELLOW}$1${TOEND}[ Warning ]"
  echo -n "${RESET}"
  echo
}

display_result(){
  if [ "$1" ]
   then
     $1;
   fi
   
  if [ $? -eq 0 ]; then
      echo -n "${GREEN}${TOEND}[ OK ]"
  else
      echo -n "${RED}${TOEND}[ fail ]"
  fi
  echo -n "${RESET}"
  echo
}

add_index_file() {
    echo "<html>
<body>
    <h1>$host works!</h1>
</body>
</html>" > $projects/$host/index.htm
}

reload_apache() {
    service apache2 reload
}

add_host() {
    echo "
<VirtualHost $host:$port>
    ServerName $host
    ServerAlias www.$host
    DocumentRoot $projects/$host
</VirtualHost>" > $hosts/$host_config
    echo "127.0.0.1        $host" >> $hosts_file
}

create_host() {
    if [ -z $host ]; then
        echo 'No name for vhost given.';
        exit 1;
    fi
    
    if [ -f "$hosts/$host_config" ]; then
        echo 'Virtual host already exists.';
        exit 0;
    fi
    
    echo 'All is ok, fast forward -->';
    #display_result add_folders;
    echo '* Folders created';
    display_result add_host;
    
    a2ensite $host_config
    display_result;
    
    echo '* Host created';
    if [ $make_index -eq 1 ]; then 
      echo ' * Created index file';
      display_result add_index_file;
    else
      echo "Dont make index file";
    fi
    
    display_result reload_apache;   
}

delete_host() {
    if [ -z $host ]; then 
        echo 'No name for vhost given. Exit.';
        exit 1;
    fi
    
    if [ -f "$hosts/$host_config" ]; then
        echo "Remove vhost $host? (y/n)";
        read confirm;
    
        if [ $confirm = 'y' ]; then 
            echo "* Removing vhost file from $hosts/$host";
            rm $hosts/$host_config
            display_result
            
            echo "* Disable $host_config"
            a2dissite $host_config
            display_result
            
            echo "* Cleaning up /etc/hosts"
            sed "/$host/d" /etc/hosts > /tmp/tmp-hosts;
            mv -f /tmp/tmp-hosts /etc/hosts
            display_result
            
            echo "* Vhost $host removed";
            
            display_result reload_apache;
        else
            echo "Skipped.";
        fi
    else
        echo 'No such vhost.';
        exit;
    fi
}

#======================#
#	  MAIN         #
#======================#

if [ $user != "root" ]; then
    echo_warning 'You must be root.';
fi

# Разбор параметров командной строки
while getopts iv OPT; do
    case "$OPT" in
	i)
	    make_index=1;
	;;
        v)
            echo "`basename $0` version 0.1"
            ;;
        \?)
            # getopts вернул ошибку
            echo "error"
            echo $USAGE
            exit 1
            ;;
    esac
done
 
# Удаляем обработанные выше параметры
shift `expr $OPTIND - 1`

# Получаем переданные аргументы
action=$1;
host=$2;
port=$3;
path=$4;
host_config=$host.conf

remote(){
  echo "ssh $user@$server 'bash -s' < $0"
}

# Обработка команды
case $action in
    create | new | make | add)
	create_host;
    ;;
    rm | del | remove | delete)
        delete_host;
    ;;
    delall)
        echo "remove all"
    ;;
    on | enable)
        echo "enable"
    ;;
    off | disable)
        echo "disable"
    ;;
    help)
        show_help;
    ;;
    remote)
	remote;
    ;;
    *)
        echo $USAGE
        exit 0;
    ;;
esac

# EOF