#!/bin/bash
projects="/var/www";
enabled_hosts='/etc/apache2/sites-enabled';
user=$(whoami);
hosts="/etc/apache2/sites-available";
action=$1;
name=$2
port=$3;

red=$(tput setf 4)
green=$(tput setf 2)
reset=$(tput sgr0)
toend=$(tput hpa $(tput cols))$(tput cub 6)

display_result(){
  if [ "$1" ]
   then
     $1;
   fi
   
  if [ $? -eq 0 ]; then
      echo -n "${green}${toend}[OK]"
  else
      echo -n "${red}${toend}[fail]"
  fi
  echo -n "${reset}"
  echo
}

add_folders() {
    #mkdir -p "$projects/$name/www";
    #mkdir -p "$projects/$name/logs"
    #mkdir -p "$projects/$name/tmp"
    chown -R $user\: "$projects/$name/"
    chmod -R 777 "$projects/$name";
}

add_host() {
    echo "
<VirtualHost $name:80>
    ServerAdmin webmaster@localhost
    ServerName $name
    ServerAlias www.$name
    DocumentRoot $projects/$name
</VirtualHost>" > $hosts/$name.conf
       
    echo "127.0.0.1        $name" >> /etc/hosts
}

add_index_file() {
    echo "<html>
<body>
    <h1>$name works!</h1>
</body>
</html>" > $projects/$name/index.htm
}

create_host() {
    if [ -z $name ]; then 
        echo 'No name for vhost given.';
        exit 1;
    fi
    
    if [ -f "$hosts/$name" ]; then
        echo 'Virtual host already exists.';
        exit 0;
    fi
    
    echo 'All is ok, fast forward -->';
    display_result add_folders;
    echo '* Folders created';
    display_result add_host;
    
    a2ensite $name.conf
    display_result;
    
    echo '* Host created';
    display_result add_index_file;
    
    
    display_result reload_apache;
    
}

delete_host() {
    if [ -z $name ]; then 
        echo 'No name for vhost given. Exit.';
        exit 1;
    fi
    
    if [ -f "$hosts/$name.conf" ]; then
        echo "Remove vhost $name? (y/n)";
        read confirm;
    
        if [ $confirm = 'y' ]; then 
            echo "* Removing vhost file from $hosts/$name";
            rm $hosts/$name.conf
            display_result
            
            echo "* Disable $name.conf"
            a2dissite $name.conf
            display_result
            
            echo "* Cleaning up /etc/hosts"
            sed "/$name/d" /etc/hosts > /tmp/tmp-hosts;
            mv -f /tmp/tmp-hosts /etc/hosts
            display_result
            
            echo "* Vhost $name removed";
            
            display_result reload_apache;
        else
            echo "Skipped.";
        fi
    else
        echo 'No such vhost.';
        exit;
    fi
}

delete_all() {
    if [ -z $name ]; then 
        echo 'No name for vhost given. Exit.';
    exit 1;
    fi
    
    if [ -f "$hosts/$name" ]; then
        echo "Remove vhost $name?";
        echo "This action will remove all files and directories from this project (y/n)";
    read confirm;
    
    if [ $confirm = 'y' ]; then 
        echo "* Removing files and directories from $projects/$name";
    rm -rf $projects/$name
    
    echo "* Removing vhost file from $hosts/$name";
    rm $hosts/$name
    
    echo "* Removing link from $enabled_hosts/$name"
    rm $enabled_hosts/$name
    
    echo "* Cleaning up /etc/hosts"
    sed "/$name/d" /etc/hosts > /tmp/tmp-hosts;
    mv -f /tmp/tmp-hosts /etc/hosts
    
    echo "* Done. The matrix has project $name";
    
    reload_apache;
    else
        echo "Skipped.";
    fi
    else
        echo 'No such vhost.';
    exit;
    fi
}

enable_host() {
    if [ -f "$hosts/$name" ]; then
        a2ensite $name.conf
        echo "127.0.0.1        $name" >> /etc/hosts
        
        reload_apache;
    else
        echo 'No such vhost.';
    fi
}

disable_host() {
    if [ -f "$enabled_hosts/$name" ]; then
        echo '* Disable current host...';
        a2dissite $name.conf
        echo '* Updating /etc/hosts';
        sed "/$name/d" /etc/hosts > /tmp/tmp-hosts;
        mv -f /tmp/tmp-hosts /etc/hosts
        
        reload_apache;
        echo '* Done'
    else
        echo 'No such vhost.';
    fi
}

show_help() {
    echo 'Available actions:
    create - creates a new project. Includes folder structure and database (optional).
    delete - delete all project files and drops database (optional). Asks for confirmation.
    enable - enables selected virtual host.
    disable - disables selected virtual host
    help - shows this message.';    
}

reload_apache() {
    service apache2 reload
}

if [ $(whoami) != "root" ]; then
    echo 'You must be root.'
    exit 1;
fi
    
case $action in 
    create)
        create_host;
    ;;
    add)
        create_host;
    ;;
    rm)
        delete_host;
    ;;
    del)
        delete_host;
    ;;
    delete)
        delete_host;
    ;;
    delall)
        delete_all;
    ;;
    on)
        enable_host;
    ;;
    enable)
        enable_host;
    ;;
    off)
        disable_host;
    ;;
    disable)
        disable_host;
    ;;
    help)
        show_help;
    ;;
    *)
        echo "Usage: `basename $0` (create|delete|enable|disable) vhostname";
        exit 0;
    ;;
esac