#! /bin/sh
 
USAGE="Usage: `basename $0` [-hv] [-o arg] args"
 
# Разбор параметров командной строки
while getopts hvo: OPT; do
    case "$OPT" in
        h)
            echo $USAGE
            exit 0
            ;;
        v)
            echo "`basename $0` version 0.1"
            exit 0
            ;;
        o)
            OUTPUT_FILE=$OPTARG
            ;;
        \?)
            # getopts вернул ошибку
            echo "error"
            #echo $USAGE &gt;&amp;2
            exit 1
            ;;
    esac
done
 
# Удаляем обработанные выше параметры
shift `expr $OPTIND - 1`
 
# Здесь мы требуем как минимум один параметр помимо опций
# Удалить блок, если он не требуется
if [ $# -eq 0 ]; then
    echo "added block"
    #echo $USAGE &gt;&amp;2
    exit 1
fi
 
# Доступ к дополнительным параметрам осуществляется через обычные
# переменные $@, $*, $1, $2, и т.д. Или используя этот цикл
for PARAM; do
    echo $PARAM
done
 
# EOF